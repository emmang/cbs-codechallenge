<?php

require_once('AuthService.php');

class AuthByQA extends AuthService{

	// Dummy credential to mimic registered user. 
	private $_qaData = array('answer' => 'chocolate');
	function __construct() {
		parent::__construct($this->_qaData);
	}

}