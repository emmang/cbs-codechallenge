<?php

require_once('AuthByPwd.php');

$postData = file_get_contents("php://input");  // get details of the POST request
$request = json_decode($postData);

$login = $request->username;
$password = $request->password;
$authByPwd = new AuthByPwd;
$user = $authByPwd->checkLogin(array('login' => $login, 'password' => $password));

if($user){
	$response = array('success' => true, 'user' => $login, 'message' => 'Login success!');
}else{
	$response = array('success' => false, 'user' => '', 'message' => 'Invalid login. Please try again.');
}

echo json_encode($response);

?>