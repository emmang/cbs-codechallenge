<?php

require_once('AuthByQA.php');

$postData = file_get_contents("php://input");  // get details of the POST request
$request = json_decode($postData);

$answer = $request->answer;
$authByQA = new AuthByQA;
$user = $authByQA->checkLogin(array('answer' => $answer));

if($user){
	$response = array('success' => true, 'message' => 'Email sent');
}else{
	$response = array('success' => false, 'message' => 'Invalid answer. Please try again.');
}

echo json_encode($response);

?>