<?php

class AuthService{

	// Store dummy credential to mimic registered user. 
	private $_loginData = array();

	function __construct($fakeCredentials = array()){
		if(empty($fakeCredentials)){  // set default credentials
			$this->_loginData['login'] = 'Barney';
			$this->_loginData['password'] = '12345'; 
		}else{
			foreach ($fakeCredentials as $key => $value) {
				$this->_loginData[$key] = $fakeCredentials[$key];
			}
		}
	}

	public function checkLogin($loginData = array()){

		$validUser = true; 

		foreach ($loginData as $key => $value) {
			if($this->_loginData[$key] !== $loginData[$key]){
				$validUser = false;
			}
		}

		if($validUser){
			$this->createUserSession();
			return true;
		}else{
			$this->destroyUserSession();
			return false;
		}
	}

	public function logout(){
		$this->destroyUserSession();
	}

	private function createUserSession(){
		session_start();
		$_SESSION['loggedIn'] = '1';
	}

	private function destroyUserSession(){
		if (isset($_SESSION) && $_SESSION['loginName']){
			session_destroy();
			unset($_SESSION['loggedIn']);
		}
	}

}

