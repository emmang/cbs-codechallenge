var userModule = angular.module('UserApp', ['ngRoute', 'ngCookies']);

// route config
userModule.config(function($routeProvider){
	$routeProvider
		.when('/', {
			templateUrl: 'loginForm.html',
			controller: 'LoginController',
			controllerAs: 'ctrl'
		})
		.when('/user', {
			templateUrl: 'loginForm.html',
			controller: 'LoginController',
			controllerAs: 'ctrl'
		})
		.when('/dashboard/:user', {
			templateUrl: 'dashboard.html',
			controller: 'DashboardController',
			controllerAs: 'ctrl'
		})
		.when('/forgetPwd', {
			templateUrl: 'forgetPwd.html',
			controller: 'ForgetPwdController',
			controllerAs: 'ctrl'
		})
		.otherwise({
			redirectTo:'/'
		});
})
.controller('UserController', ['$scope', function($scope){

}])
.controller('LoginController', ['$scope', '$http', '$location', '$cookies', 
	function($scope, $http, $location, $cookies){

		this.user = {
			name: null,
			password: null
		};

		this.error = null;
		$cookies.remove('auth_username');

		this.doLogin = function(){
			var self = this;  // scoping issue with Angular
			$http({
				method: 'post',
				url: '../login.php',
				data: {
					username: this.user.name,  // correct username: Barney
					password: this.user.password  // correct password: 12345
				}
			}).then(function(res){
				var data = res.data;
				if(data.success){
					$cookies.put('auth_username', data.user);
					$location.path('/dashboard/' + data.user); // render dashboard view
				}else{
					self.error = data.message;
				}
	        }, function(res){
	            console.log('Error submitting login request');
	        });
		};

		this.doForgetPwd = function(){
			$location.path('/forgetPwd');
		};

		this.clearError = function(){  // remove errors on form
			this.error = null;
		};

}])
.controller('DashboardController', ['$scope', '$http', '$routeParams', '$cookies', '$location', 
	function($scope, $http, $routeParams, $cookies, $location){

		if(!$cookies.get('PHPSESSID') || !$cookies.get('auth_username')){
			$location.path('/');
		}

		this.user = $routeParams.user || 'User';

		this.doLogout = function(){
			$http({
				method: 'post',
				url: '../logout.php'
			}).then(function(res){
				$cookies.remove('auth_username');
				$location.path('/');
	        }, function(res){
	            console.log('Error submitting logout request');
	        });
		}
}])
.controller('ForgetPwdController', ['$scope', '$http', '$location', '$cookies', 
	function($scope, $http, $location, $cookies){

		this.user = {
			answer: null
		};

		this.error = null;
		this.message = null;
		$cookies.remove('auth_username');

		this.doLogin = function(){
			var self = this;  // scoping issue with Angular
			$http({
				method: 'post',
				url: '../loginByQA.php',
				data: {
					answer: this.user.answer  // correct answer: chocolate
				}
			}).then(function(res){
				var data = res.data;
				if(data.success){
					self.message = 'Email has been sent to you with your temporary password.';
				}else{
					self.error = data.message;
				}
	        }, function(res){
	            console.log('Error submitting login request');
	        });
		};

		this.clearAlert = function(){
			this.error = null;
			this.message = null;
		};

}]);


