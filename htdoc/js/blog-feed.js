var blogAppModule = angular.module('BlogApp', []);

// Filter for blog description that contains HTML markup
blogAppModule.filter('unsafe', function($sce){
    return function(val){
        return $sce.trustAsHtml(val);
    }
});

blogAppModule.directive('whenscrolling', function(){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs){
            var raw = elem[0];
            elem.bind('scroll', function(){
                // fetch more blog articles
                if(raw.scrollTop + raw.offsetHeight+5 >= raw.scrollHeight){
                    scope.loading = true;
                    scope.$apply(attrs.whenscrolling);
                }
            });
        }
    }
});

blogAppModule.controller('BlogAppController', ['$scope', '$http', function($scope, $http){

        var currentPage = 1; // current page size number
        var maxPageSize = 1; // max set of blogs we can fetch
        $scope.blogItems = [];
        $scope.loading = true;
        
        $scope.loadBlogItems = function() {

            if(currentPage <= maxPageSize){
                $http.get('../getFeed.php?page=' + currentPage).then(function(res){
                    $scope.blog = res.data;
                    for(var i=0; i < res.data.item.length; i++){
                        $scope.blogItems.push(res.data.item[i]);
                    }
                    $scope.loading = false;
                    maxPageSize = res.data.maxPageSize;
                }, function(resp){
                    console.log('Error getting blog json');
                });
                currentPage++;
            }
        };

        $scope.loadBlogItems();

}]);

