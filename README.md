# CBS Coding Challenge #


### 1. Create a simple blog engine with infinite scrolling (auto loads next batch of content when you reach the bottom), in PHP/JS. Use any RSS/JSON feed for content. ###

* Open htdoc/blog.html
* Related Files:

* * htdoc/blog.html
* * htdoc/css/style.css
* * getFeed.php
* * RSSfeed.php

### 2. Implement basic authentication in PHP/JS. Don't refresh the page on form submit. If user/pass is correct - display user’s full name returned from server, save auth in cookies. If incorrect - display error message. Use OOP, extend main class and override authentication function (for ex. save auth to another cookie or use another authentication method). ###

* Open htdoc/user.html
* Login name: Barney
* Password: 12345
* Forget password answer: chocolate
* Related Files:

* * htdoc/user.html
* * htdoc/loginForm.html
* * htdoc/forgetPwd.html
* * htdoc/dashboard.html
* * htdoc/js/user.js
* * htdoc/css/style.css
* * AuthByPwd.php
* * AuthByQA.php
* * AuthService.php
* * login.php
* * loginByQA.php
* * logout.php