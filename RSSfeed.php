<?php

class RSSFeed{

	private $_url = 'http://aroundtheworldblog.blogspot.com/feeds/posts/default?alt=rss';
	private $_feedArr;
	private $_feedArrLength;
	private $_jsonFeed;
	private $_pageSize = 3;
	private $_maxPageSize = 0;

	public function init(){
		$this->_buildBlogArray();
	}

	private function _buildBlogArray(){
		header('Content-Type: application/json; Charset=UTF-8');
		$feed = new DOMDocument();
		$feed->load($this->_url); 

		$this->_feedArr['title'] = $feed->getElementsByTagName('channel')->item(0)->getElementsByTagName('title')->item(0)->firstChild->nodeValue;
		$this->_feedArr['description'] = $feed->getElementsByTagName('channel')->item(0)->getElementsByTagName('description')->item(0)->firstChild->nodeValue;
		$this->_feedArr['link'] = $feed->getElementsByTagName('channel')->item(0)->getElementsByTagName('link')->item(0)->firstChild->nodeValue;
		$items = $feed->getElementsByTagName('channel')->item(0)->getElementsByTagName('item');
		$this->_feedArr['item'] = array();

		foreach($items as $item) {
		   $title = $item->getElementsByTagName('title')->item(0)->firstChild->nodeValue;
		   $description = $item->getElementsByTagName('description')->item(0)->firstChild->nodeValue;
		   $pubDate = $item->getElementsByTagName('pubDate')->item(0)->firstChild->nodeValue;
		   $guid = $item->getElementsByTagName('guid')->item(0)->firstChild->nodeValue;
		   $this->_feedArr['item'][] = array('title'=>$title,'description'=>$description, 'pubdate'=>$pubData, 'guid'=>$guid);
		}

		$this->_feedArrLength = count($this->_feedArr['item']);
		$this->_maxPageSize = ceil($this->_feedArrLength / $this->_pageSize);
		$this->_feedArr['maxPageSize'] = $this->_maxPageSize;
	}

	public function getJsonFeed($page=1){  // return set of blog articles (pageSize 3 at a time)

		$feedCopy = array();

		if($page <= $this->_maxPageSize){
			$start = $page > 1 ? ($page - 1) * $this->_pageSize : 0;
			$size = ($start + $this->_pageSize) > $this->_feedArrLength ? $this->_feedArrLength - 1 : $this->_pageSize;
			$feedCopy = array_merge(array(), $this->_feedArr);
			$requestedfeedItems = array_slice($feedCopy['item'], $start, $size); 
			$feedCopy['item'] = $requestedfeedItems;
		}

		return json_encode($feedCopy);
	}
}

?>